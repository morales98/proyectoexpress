var mongoose = require('mongoose'); //modulo 3. video 2. 

const Schema = mongoose.Schema;
const TokenSchema = new Schema({
	_userId:{type: mongoose.Schema.Types.ObjectId, require:true, ref: 'Usuario'},
	token: {type: String, required: true},
	createdAt: {type: Date, required: true, default: Date.now, expires: 43200}
}); 

/*guardar el id de user. Referencia al objeto de la coleccion de usuarios en mongo. 
por medio del objetid consultando en usuario dentro del token que es para validad una 
cuenta de usuario*/

module.exports = mongoose.model("Token", TokenSchema);