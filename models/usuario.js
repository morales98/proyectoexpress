var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');

var Schema = mongoose.Schema; 
const crypto = require('crypto');
const bcrypt = require('bcrypt'); //modulo que permite encriptar
const saltRounds = 10; //aleatoriedad a la encriptacion

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

const validateEmail = function(email){
	const re = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i; // regex para verificar email
	return re.test(email);
};

var userSc = new Schema({
	nombre : {
		type: String,
		trim: true, //sacar espacios vacion
		required: [true, 'El nombre es obligatorio']
	},

	email: {
		type: String, 
		trim:true,
		required: [true, 'El email es obligatorio'],
		lowercase: true,
		unique: true,
		validate: [validateEmail, 'Por favor, ingrese un email valido'],
		match: [/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i] // corre a nivel mongo
	} ,

	password: {
		type: String,
		required: [true, 'La contraseña es obligatoria']
	},
	
	passwordResetToken: String, //terminos tecnicos
	passwordResetTokenExpires: Date,

	verificado: { //verificacion de cuenta por email
		type: Boolean, 
		default: false
	},
	googleId: String,
	facebookId: String
});

//plugin no es de mongoose. es algo extra a las librerias.

userSc.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'});


//encriptar contraseñas
userSc.pre('save', function(next){ //antes de save se ejecuta esta funcion
	if(this.isModified('password')){ //verifica si la contraaseñoa cambio
		this.password= bcrypt.hashSync(this.password, saltRounds);
	}
	next();
});

userSc.methods.validPassword = function(password){
	return bcrypt.compareSync(password, this.password); //se usa es en loging
}

userSc.methods.reservar = function(biciId, desde, hasta, cb){
	var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
	console.log(reserva);
	reserva.save(cb);
}
//
userSc.methods.enviar_email_bienvenida =function(cb){
	const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
	const email_destination = this.email; 
	token.save(function(err){
		if (err) {return console.log(err.message);}

		const mailOptions ={
			from: 'no-reply@redbicicletas.com',
			to: email_destination,
			subject: 'verificacion de cuenta',
			text: 'Hola, \n\n' + 'Por favor, verfica tu cuenta haciendo click en el link \n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '\n'

		};

		mailer.sendMail(mailOptions, function(err){
			if(err) {return console.log(err.message);}

			console.log('Un email de verificacion se ha enviado a' + email_destination + '.');
		});
	});
}


userSc.methods.resetPassword = function(cb){
	const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
	const email_destination = this.email; 
	token.save(function(err){
		if (err) {return cb(err); }

		const mailOptions = {
			from: 'no-reply@redbicicletas.com',
			to: email_destination,
			subject: 'Nueva contraseña',
			text: 'Hola, \n\n' + 'Para resetear la contraseña de su cuenta haga clic en este link: \n' + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '.\n'
		};
		mailer.sendMail(mailOptions, function(err){
			if(err) {return cb(err); }
			console.log('Se envio un email para cambiar la contraseña a: ' + email_destination + '.');
		});
		cb(null);
	});
}

userSc.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){ // video 3 de google oauth
	const self = this; 
	console.log(condition);
	self.findOne({
		$or:[
		{'googleId': condition.id}, {'email': condition.emails[0].value}
		]}, (err, result)=> {
		if (result) { // login
			callback(err,result)
		}else{
			console.log('............. CONDITION .............'); // registro
			console.log(condition);
			let values = {};
			values.googleId = condition.id;
			values.email = condition.emails[0].value; 
			values.nombre = condition.displayName || 'SIN NOMBRE';
			values.verificado = true;
			values.password = condition._json.sub; //objeto que representa la clase de conexion
			console.log('............. VALUES .............');
			console.log(values);
			self.create(values, (err, result) => {
				if (err) {console.log(err);}
				return callback(err, result)
			})
		}

	})
};

userSc.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){ // video 3 de google oauth
	const self = this; 
	console.log(condition);
	self.findOne({
		$or:[
		{'facebookId': condition.id}, {'email': condition.emails[0].value}
		]}, (err, result)=> {
		if (result) { // login
			callback(err,result)
		}else{
			console.log('............. CONDITION .............'); // registro
			console.log(condition);
			let values = {};
			values.facebookId = condition.id;
			values.email = condition.emails[0].value; 
			values.nombre = condition.displayName || 'SIN NOMBRE';
			values.verificado = true;
			values.password = crypto.randomBytes(16).toString('hex'); //objeto que representa la clase de conexion
			console.log('............. VALUES .............');
			console.log(values);
			self.create(values, (err, result) => {
				if (err) {console.log(err);}
				return callback(err, result)
			})
		}

	})
};
//
module.exports = mongoose.model('Usuario',userSc);