var nodemailer = require('nodemailer'); //email locales
const sgTransport = require('nodemailer-sendgrid-transport'); // email remoto
//var dotenv = require('dotenv')
//
//crear configuraciones del envio de mails

let mailConfig;
if (process.env.node_env === 'production'){
	console.log('ambiente heroku')
	const options = {
		auth: {
			api_key: process.env.SENDGRID_API_SECRET
		}
	}
	mailConfig = sgTransport(options);

}else{
	if (process.env.node_env === 'staging') {
		console.log('XXXXXXXXXXXXXXXXXXXXX');
		const options ={
			auth:{
				api_key: process.env.SENDGRID_API_SECRET
			}
		}
		mailConfig = sgTransport(options);

	}else{
		console.log('ambiente local');
		// emails en ethereal
		mailConfig = {
			host: 'smtp.ethereal.email',
    		port: 587,
    		auth: {
				user: process.env.ethereal_user,
				pass: process.env.ethereal_pwd 
  			}
		};
	}
}
/*
const mailConfig ={
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'allen.satterfield13@ethereal.email',
        pass: 'ENB9seQbd4GUE1Hp3H'
    }
};

// se instala nodemailer-sendgrid-transport para enviar emails a personas reales.
// se crea una cuenta en sengrid.com
*/
module.exports= nodemailer.createTransport(mailConfig);