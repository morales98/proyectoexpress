require('newrelic');
require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const passport = require('./config/passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuarioAPIRouter = require('./routes/api/usuarios');
var authAPIRouter = require('./routes/api/auth');

const Usuario = require('./models/usuario');
const Token = require('./models/token');

//const store = new session.MemoryStore; //en memoria del servidor, se pierden los datos de sesion

let store; 
if (process.env.node_env === 'desarrollo'){
	store = new session.MemoryStore; 
} else {
	store = new MongoDBStore({
		uri: process.env.mongo_url,
		collection: 'sessions'
	});
	store.on('error', function(error){
		assert.ifError(error);
		assert.ok(false);
	});
}

let app = express();
app.set('secretKey', 'jwt._pwd_!!951357');
app.use(session({
	cookie: {maxAge: 240*60*60*1000}, //ms
	store: store,
	saveUninitialized: true,
	resave: 'true',
	secret: 'red_bicis_aasdsjfdhdj' //codigo para encriptacion de identificacion de la cookie
}));

var mongoose = require('mongoose'); // modulo 2

//var mongoDB = 'mongodb://localhost/red-bicicletas';
//var mongoDB = 'mongodb+srv://admin:oFuquAbEQC5JnEB2@red-bicicletas.aa3l5.mongodb.net/<mongoDB>?retryWrites=true&w=majority';
//variables de entorno; se decide cual usar en funcion del entorno actual. 
var mongoDB = process.env.mongo_url; 
mongoose.connect(mongoDB, {useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error',console.error.bind(console,'MongoDB connection error: ')); // modulo 2

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function(req,res){
	res.render('session/login');
});

app.post('/login', function(req,res,next){ // profundizar en la documentacion de password
	passport.authenticate('local', function(err,usuario,info){
		if(err) return next(err);
		if(!usuario) return res.render('session/login', {info});
		req.logIn(usuario,function(err){
			return res.redirect('/');
		});

	})(req,res,next);
});

app.get('/logout', function(req,res){
	req.logout();
	res.redirect('/');
});

/*app.get('/forgotPassword', function(req,res){

});

app.post('/forgotPassword', function(req,res){

});*/

app.get('/forgotPassword', function(req, res) {
	res.render('session/forgotPassword');
});

app.post('/forgotPassword', function(req, res) { 
	Usuario.findOne({ email: req.body.email}, function (err, usuario) { 
		if (!usuario) return res.render('session/forgotPassword', {info: {message: 'No existe el email para un usuario existente.'}});

		usuario.resetPassword(function(err) { 
			if (err) return next(err); 
			console.log('session/forgotPasswordMessage');
		});

		res.render('session/forgotPasswordMessage');

	});

});

app.get('/resetPassword/:token', function(req, res, next) { 
	Token.findOne({ token: req.params.token }, function (err, token) {
		if (!token) return res.status(400).send({ type: 'not-verified', message: 'No existe un usuario asociado al token. Verifique que su token no haya expirado.' });
	
		Usuario.findById(token._userId, function (err, usuario) {
		 	if (!usuario) return res.status(400).send({ msg: 'No existe un usuario asociado al token.' });
			res.render('session/resetPassword', {errors: {}, usuario: usuario});
		});
	}); 
});

app.post('/resetPassword', function(req,res){
	if(req.body.password != req.body.confirm_password){
		res.render('session/resetPassword', {errors: {confirm_password: {message: 'No coincide con la constrañse ingresada'}}, usuario: new Usuario({email:req.body.email})});
		return;
	}
	Usuario.findOne({email:req.body.email}, function (err,usuario){
		usuario.password = req.body.password;
		usuario.save(function(err){
			if(err){
				res.render('session/resetPassword', {errors:err.errors,usuario: new Usuario({email:req.body.email})});
			}else{ 
				res.redirect('/login');
			}});	
	});
});
		
app.use('/privacy_policy', function(req, res){
	res.sendFile('public/privacy_policy.html');
});

app.use('/google9a6751228d8224f4', function(req, res){
	res.sendFile('public/google9a6751228d8224f4.html');
});

app.get('/auth/google',
  passport.authenticate('google', { scope: 
      [ 'https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/userinfo.email' ] }));
app.get( '/auth/google/callback', passport.authenticate( 'google', { 
        successRedirect: '/',
        failureRedirect: '/error'
    })
);

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);

//el usuario solo ingresara a bicicletas si esta loggeado. Cool. 
app.use('/bicicletas', loggedIn, bicicletasRouter);

app.use('/api/auth/', authAPIRouter)

app.use('/api/bicicletas', validateUser, bicicletasAPIRouter);
app.use('/api/usuarios', usuarioAPIRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//permite autorizar al usuario a alguna ruta. 

function loggedIn(req,res,next){
	if(req.user){
		console.log(req.user);
		next();
	}else{
		console.log('Usuario sin logearse');
		res.redirect('/login');
	};
};

function validateUser(req,res,next) {
	jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
		if(err) {
			res.json({status:"error", message: err.message, data:null});
		}else{

			req.body.userId = decoded.id;
			console.log('jew verify: ' +decoded);
			next();
		}
	});
}

module.exports = app;
